/*global jQuery, Handlebars, Router */
jQuery(function ($) {
	'use strict';

	Handlebars.registerHelper('eq', function (a, b, options) {
		return a === b ? options.fn(this) : options.inverse(this);
	});

	var ENTER_KEY = 13;
	var ESCAPE_KEY = 27;

	var util = {
		uuid: function () {
			/*jshint bitwise:false */
			var i, random;
			var uuid = '';

			for (i = 0; i < 32; i++) {
				random = Math.random() * 16 | 0;
				if (i === 8 || i === 12 || i === 16 || i === 20) {
					uuid += '-';
				}
				uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
			}

			return uuid;
		},
		pluralize: function (count, word) {
			return count === 1 ? word : word + 's';
		},
		store: function (namespace, data) {
			if (arguments.length > 1) {
				return localStorage.setItem(namespace, JSON.stringify(data));
			} else {
				var store = localStorage.getItem(namespace);
				return (store && JSON.parse(store)) || [];
			}
		}
	};

	var App = {
		init: function () {
			this.todos = util.store('todos-jquery');
			this.todoTemplate = Handlebars.compile($('#todo-template').html());
			this.footerTemplate = Handlebars.compile($('#footer-template').html());
			this.bindEvents();

			new Router({
				'/:filter': function (filter) {
					this.filter = filter;
					this.render();
				}.bind(this)
			}).init('/all');
		},
		bindEvents: function () {
			$('#new-todo').on('keyup', this.create.bind(this)); // 120 min. reading - wrap HTML element in jQuery. Attach create method to keyup event on this element, bind this to App object.
			$('#toggle-all').on('change', this.toggleAll.bind(this));
			$('#footer').on('click', '#clear-completed', this.destroyCompleted.bind(this));
			$('#todo-list')
				.on('change', '.toggle', this.toggle.bind(this))
				.on('dblclick', 'label', this.edit.bind(this))
				.on('keyup', '.edit', this.editKeyup.bind(this))
				.on('focusout', '.edit', this.update.bind(this))
				.on('click', '.destroy', this.destroy.bind(this));
		},
		render: function () {
			var todos = this.getFilteredTodos();
			$('#todo-list').html(this.todoTemplate(todos));
			$('#main').toggle(todos.length > 0);
			$('#toggle-all').prop('checked', this.getActiveTodos().length === 0);
			this.renderFooter();
			$('#new-todo').focus();
			util.store('todos-jquery', this.todos);
		},
		renderFooter: function () {
			var todoCount = this.todos.length;
			var activeTodoCount = this.getActiveTodos().length;
			var template = this.footerTemplate({
				activeTodoCount: activeTodoCount,
				activeTodoWord: util.pluralize(activeTodoCount, 'item'),
				completedTodos: todoCount - activeTodoCount,
				filter: this.filter
			});

			$('#footer').toggle(todoCount > 0).html(template);
		},
		toggleAll: function (e) {
			var isChecked = $(e.target).prop('checked');

			this.todos.forEach(function (todo) {
				todo.completed = isChecked;
			});

			this.render();
		},
		getActiveTodos: function () {
			return this.todos.filter(function (todo) { // 120 min. reading - call .filter method with todo as parameter on todos and return its value.
				return !todo.completed; // 120 min. reading - return todos with completed property of false.
			});
		},

		getCompletedTodos: function () {
			return this.todos.filter(function (todo) { 
				return todo.completed; // 120 min. reading - return todos with completed property of true.
			});
		},
		getFilteredTodos: function () {
			if (this.filter === 'active') { // 120 min. reading - if filter is set to active,
				return this.getActiveTodos(); // 120 min. reading - return value that was returned by getActiveTodos call.
			}

			if (this.filter === 'completed') { // 120 min. reading - if filter is set to completed,
				return this.getCompletedTodos(); // 120 min. reading - return value that was returned by getCompletedTodos call. 
			}

			return this.todos; // 120 min. reading - else return todos.
		},
		destroyCompleted: function () {
			this.todos = this.getActiveTodos(); // 120 min. reading - set todos array to return value of getActiveTodos call.
			this.filter = 'all'; // 120 min. reading - change filter to all.
			this.render(); // 120 min. reading - call render method.
		},
		// accepts an element from inside the `.item` div and
		// returns the corresponding index in the `todos` array
		indexFromEl: function (el) {
			var id = $(el).closest('li').data('id'); // 120 min. reading - wrap method parameter in jQuery object and find its closest parent li element, and set variable id to its data-id value. 
			var todos = this.todos; // 120 min. reading - set variable todos to this.todos.
			var i = todos.length; // 120 min. reading - set variable i to todos array length.

			while (i--) { // 120 min. reading - while i is not a negative number, run if statement. If it is not true i-- and do it again.
				if (todos[i].id === id) { // 120 min. reading - check if todo with position of i id property is equal to variable id,
					return i; // 120 min. reading - return i
				}
			}
		},
		create: function (e) {
			var $input = $(e.target); // 120 min. reading - set variable $input to event target wrapped in jQuery object.
			var val = $input.val().trim(); // 120 min. reading - get current value of $el and trim its whitespaces from beggining and end, and set it to variable val.

			if (e.which !== ENTER_KEY || !val) { // 120 min. reading - if keyboard event is not equal to variable ENTER_KEY or val is false,
				return; // 120 min. reading - exit the method.
			}

			this.todos.push({ // 120 min. reading - call push method on todos array.
				id: util.uuid(), // 120 min. reading - set id property to return value of util.uuid.
				title: val, // 120 min. reading - set title property to variable val.
				completed: false // 120 min. reading - set completed property to false.
			});

			$input.val(''); // 120 min. reading - set $input value to empty string.

			this.render(); // 120. min reading - call reder method.
		},
		toggle: function (e) { // 120 min. reading - accept event as argument.
			var i = this.indexFromEl(e.target); // 120 min. reading - set variable i to return value of indexFromEl(event.target).
			this.todos[i].completed = !this.todos[i].completed; // 120 min. reading - set todo with position i completed property to opposite to what it is now.
			this.render(); // 120. min reading - call reder method.
		},
		edit: function (e) { // 120 min. reading - accept event as argument.
			var $input = $(e.target).closest('li').addClass('editing').find('.edit'); // 120 min. reading - wrap event target in jQuery object and find its closest parent li element. Set its class to editing. Now look for a child element with class edit and set it to variable $input. 
			$input.val($input.val()).focus(); // 120 min. reading - get current value of variable $input and call focus method on it - gain keyboard focus.
		},
		editKeyup: function (e) { // 120 min. reading - accept event as argument.
			if (e.which === ENTER_KEY) { // 120 min. reading - if keyboard event is equal to variable ENTER_KEY,
				e.target.blur(); // // 120 min. reading - call blur method on event target - remove keyboard focus.
			}

			if (e.which === ESCAPE_KEY) { // 120. reading - if keyboard event is equal to variable ESCAPE_KEY,
				$(e.target).data('abort', true).blur(); // 120 min. reading - wrap event target in jQuery object, set its arbitrary data abort value to true and call blur method on it - remove keyboard focus.
			}
		},
		update: function (e) { // 120 min. reading - accept event as argument.
			var el = e.target; // 120 min. reading - set variable el to event target.
			var $el = $(el); // 120 min. reading - set variable $el to el wrapped in jQuery object.
			var val = $el.val().trim(); // 120 min. reading - get current value of $el and trim its whitespaces from beggining and end, and set it to variable val.

			if (!val) { // 120 min. reading - if val value is false,
				this.destroy(e); // 120 min. reading - run destroy method with event as argument.
				return; // 120 min. reading - exit method.
			}

			if ($el.data('abort')) { // 120 min. reading - if $el arbitrary data abort is true,
				$el.data('abort', false); // 120 min. reading - set $el arbitrary data abort to false.
			} else { // 120 min. reading - else,
				this.todos[this.indexFromEl(el)].title = val; // 120. min reading - access title property value of todo in todos array with position of return value of indexFromEl(el) and set it to val.
			}

			this.render(); // 120. min reading - call reder method.
		},
		destroy: function (e) { // 120 min. reading - accept event as argument.
			this.todos.splice(this.indexFromEl(e.target), 1); // 120 min. reading - call splice method on this/App.todos with first argument of return value of indexFromEl(event.target) and second 1. 
			this.render(); // 120. min reading - call reder method.
		}
	};

	App.init();
});
