/*global jQuery, Handlebars, Router */
jQuery(function ($) {
	// registering a custom Handlebars helper.
	Handlebars.registerHelper('eq', function (a, b, options) { 
		return a === b ? options.fn(this) : options.inverse(this); // if a === b use options.fn, else use options.inverse (we do not have else defined in this app).
	});

	var ENTER_KEY = 13; // set variable ENTER_KEY to number 13 or keyboard button ENTER.
	var ESCAPE_KEY = 27; // set variable ESCAPE_KEY to number 27 or keyboard button ESCAPE.

	var util = {
		uuid: function () {
			/*jshint bitwise:false */
			var i, random;
			var uuid = '';

			for (i = 0; i < 32; i++) {
				random = Math.random() * 16 | 0;
				if (i === 8 || i === 12 || i === 16 || i === 20) {
					uuid += '-';
				}
				uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
			}

			return uuid; // return uuid value.
		},
		pluralize: function (count, word) { // method to pluralize word item, if todos count is more than 1.
			return count === 1 ? word : word + 's'; // if count is 1 return word, else return words.
		},
		// if method argument length is more than 1, save data to, else get data from local storage.
		store: function (namespace, data) {
			if (arguments.length > 1) { 
				return localStorage.setItem(namespace, JSON.stringify(data)); // return stringified data to local storage under specific key.
			} else { 
				var store = localStorage.getItem(namespace); // set variable store to data in local storage under specific key.
				return (store && JSON.parse(store)) || []; // if store and parsed store are truthy, return it, else return empty array.  
			}
		}
	};

	var app = {
		init: function () {
			this.todos = util.store('todos-jquery'); // get data from local storage with todos-jquery as key and set it to variable todos. 
			this.todoTemplate = Handlebars.compile($('#todo-template').html()); // compile HTML #todo-template to Handlebars todoTemplate template.
			this.footerTemplate = Handlebars.compile($('#footer-template').html()); // // compile HTML #footer-template to Handlebars todoTemplate template.
			this.bindEvents(); // call bindEvents method.

      new Router({ // setting up a new router. Connect code with URL.
				'/:filter': [function (filter) { // set variable filter to current end of URL (#/variable).
					this.filter = filter; // set variable this.filter to filter defined in previous line.
					this.render(); // call render method.
				}.bind(this)] // Bind this to app.
			}).init('/all'); // when init runs, start on /all page.      
		},
		// binds events on HTML elements.
		bindEvents: function () {
			$('#new-todo').on('keyup', this.create.bind(this)); // select #new-todo HTML element with jQuery, call create method on keyup event. Bind this to app.
			$('#toggle-all').on('change', this.toggleAll.bind(this)); // select #toggle-all HTML element with jQuery, call toggleAll method on change event. Bind this to app. 
			$('#footer').on('click', '#clear-completed', this.destroyCompleted.bind(this)); // select #footer HTML element with jQuery, call destroyCompleted method on click event on #clear-completed HTML element. Bind this to app.
			$('#todo-list') // select #todo-list HTML element with jQuery, method chain multiple events,
				.on('change', '.toggle', this.toggle.bind(this)) //call toggle method on change event on .toggle HTML element. Bind this to app.
				.on('dblclick', 'label', this.edit.bind(this)) // call edit method on dblclick event on label HTML element. Bind this to app.
				.on('keyup', '.edit', this.editKeyup.bind(this)) // call editKeyup method on keyup event on .edit HTML element. Bind this to app.
				.on('focusout', '.edit', this.update.bind(this)) // call update method on focusout event on .edit HTML element. Bind this to app.
				.on('click', '.destroy', this.destroy.bind(this)); // call destroy method on click event on .destroy HTML element. Bind this to app.
		},
		render: function () {
			var todos = this.getFilteredTodos(); // set variable todos to return value of getFilteredTodos.
			$('#todo-list').html(this.todoTemplate(todos)); // select #todo-list with jQuery, fill its inner html Handlebars todoTemplate with todos passed as data.
			$('#main').toggle(todos.length > 0); // show #main only if todos length is more than 0.
			$('#toggle-all').prop('checked', this.getActiveTodos().length === 0); // select #toggle-all with jQuery, change its inner checked property depending if this.getActiveTodos().length === 0 is true or false.
			this.renderFooter(); // call renderFooter method.
			$('#new-todo').focus(); // select #new-todo with jQuery and call focus method on it.
			util.store('todos-jquery', this.todos); // save todos array to local storage under todos-jquery key.
		},
		renderFooter: function () {
			var todoCount = this.todos.length; // set variable todoCount to todos array length.
			var activeTodoCount = this.getActiveTodos().length; // set variable activeTodosCount to return value of getActiveTodos method length.
			var template = this.footerTemplate({ // setting up footerTemplate variables for Handlebars to use.
				activeTodoCount: activeTodoCount,
				activeTodoWord: util.pluralize(activeTodoCount, 'item'),
				completedTodos: todoCount - activeTodoCount,
				filter: this.filter // set filter to current value of this.filter.
			});

			$('#footer').toggle(todoCount > 0).html(template); // show #footer only if todoCount is more than 0, set its html inner value to template.
		},
		toggleAll: function (e) { // accept jQuery event object as argument.
			var isChecked = $(e.target).prop('checked'); // select event target with jQuery, access its checkbox current value and set it as variable isChecked.

			this.todos.forEach(function (todo) { // run this function for each element in todos array.
				todo.completed = isChecked; // change todo completed property value to isChecked.
			});

			this.render(); // call render method.
		},
		// return a newly created array containing only todos with completed property of false.
		getActiveTodos: function () {
			return this.todos.filter(function (todo) { // call .filter method on todos array with argument of todo. Return this value.
				return !todo.completed; // return todos with completed value of false.
			});
		},
		// return a newly created array containing only todos with completed property of true.
		getCompletedTodos: function () {
			return this.todos.filter(function (todo) { // call .filter method on todos array with argument of todo. Return this value.
				return todo.completed; // return todos with completed value of true.
			});
		},
		// return todos depending on the filter currently active.
		getFilteredTodos: function () {
			if (this.filter === 'active') { // if filter is equal to active,
				return this.getActiveTodos(); // return the value returned by getActiveTodos method.
			}

			if (this.filter === 'completed') { // if filter is equal to completed,
				return this.getCompletedTodos(); // return value returned by getCompletedTodos method.
			}

			return this.todos; // Else return todos array.
		},
		destroyCompleted: function () {
			this.todos = this.getActiveTodos(); // set todos array to return value of getActiveTodos.
			this.filter = 'all'; // change filter to all.
			this.render(); // call render method.
		},
		// accepts an element from inside the `.item` div and
		// returns the corresponding index in the `todos` array
		indexFromEl: function (el) { // accept an element as argument.
			var id = $(el).closest('li').data('id'); // select argument with jQuery, then look for closest parent li HTML element and access its data-id value, and set it as variable id.
			var todos = this.todos; // set variable todos to todos array.
			var i = todos.length; // set variable i to todos array length.

			while (i--) { // run a loop while i is true, decrease i value by 1 each time the loop runs.
				if (todos[i].id === id) { // if id property of todo in todos array with index position of i is equal to variable id.
					return i; // return i.
				}
			}
		},
		create: function (e) { // accept jQuery event object as argument.
			var $input = $(e.target); // set variable $input to event target selected with jQuery.
			var val = $input.val().trim(); // set variable val to $input current value with whitespaces trimmed.

			if (e.which !== ENTER_KEY || !val) { // if the event was not called with ENTER or val value is empty string ('')/falsy,
				return; // exit create method.
			}
			// else
			this.todos.push({ // call push method on todos array to create a new todo.
				id: util.uuid(), // set id property to return value of uuid method.
				title: val, // set title property to variable val.
				completed: false // set completed property to false.
			});

			$input.val(''); // change $input current value to ''·

			this.render(); // call render method.
		},
		toggle: function (e) { // accept jQuery event object as argument.
			var i = this.indexFromEl(e.target); // set variable i to return value of indexFromEl(event.target).
			this.todos[i].completed = !this.todos[i].completed; // access completed property of todo with index position of i in todos array, flip this value to opposite.
			this.render(); // call render method.
		},
		edit: function (e) { // accept jQuery event object as argument.
			var $input = $(e.target).closest('li').addClass('editing').find('.edit'); // select event target with jQuery, then look for closest parent li HTML element and add class editing to it, then find child HTML element with class edit and set variable $input to it. 
			$input.focus(); // call focus method on $input.
		},
		editKeyup: function (e) { // accept jQuery event object as argument.
			if (e.which === ENTER_KEY) { // if the event was called with ENTER key,
				e.target.blur(); // call blur method on event target, cancel focus given by edit method.
			}

			if (e.which === ESCAPE_KEY) { // if the event was called with ESCAPE key,
				$(e.target).data('abort', true).blur(); // select event target with jQuery, store arbitrary data value of true under abort key and call blur method, cancel focus given by edit method.
			}
		},
		update: function (e) { // accept jQuery event object as argument.
			var el = e.target; // set variable el to event target.
			var $el = $(el); // set variable $el to el selected with jQuery.
			var val = $el.val().trim(); // set variable val to $el current value with whitespaces trimmed.

			if (!val) { // if val is empty string ('')/falsy,
				this.destroy(e); // call destroy method
				return; // exit update method.
			}

			if ($el.data('abort')) { // if $el arbitrary data under abort key is true,
				$el.data('abort', false); // set its value to fasle.
			} else { // else
				this.todos[this.indexFromEl(el)].title = val; // access title property of a todo with index position of return value of indexFromEl(event target) in todos array and set it to variable val.
			}

			this.render(); // call render method.
		},
		destroy: function (e) { // call with jQuery event object as argument.
			this.todos.splice(this.indexFromEl(e.target), 1); // call splice method on todos array with index position of returned value by indexFromEl(event.target), delete one item.
			this.render(); // call render method.
		}
	};

	app.init(); // call init method in app object.
});