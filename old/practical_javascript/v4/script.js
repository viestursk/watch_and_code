makeSandwitchWith ____
  Get slice of bread.
  Add ____.
  Put a slice of bread on top.

function makeSandwitchWith(filling){
  get one slice of bread;
  Add filling;
  Put a slice of bread on top;
}

var todos = ['item 1', 'item 2', 'item 3']

function displayTodos() {
  console.log('My todos:', todos);
}

  displayTodos()

function addTodo(todo) {
  todos.push(todo);
  displayTodos();
}

  addTodo('new task')

function changeTodo(position, newValue) {
  todos[position] = newValue;
  displayTodos();
}

  changeTodo(0, 'change task')

function deleteTodo(position) {
  todos.splice(position, 1);
  displayTodos();
}

  deleteTodo(3)
