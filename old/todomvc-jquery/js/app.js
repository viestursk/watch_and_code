/*global jQuery, Handlebars, Router */
jQuery(function ($) {
	'use strict';

	Handlebars.registerHelper('eq', function (a, b, options) { // register a new handlebars helper.
		return a === b ? options.fn(this) : options.inverse(this); // if a is equal to b, show standart template, else show different template.
	});

	var ENTER_KEY = 13; // set var. 'ENTER_KEY' to keyboard key nr. 13 - ENTER.
	var ESCAPE_KEY = 27; // set var. 'ESCAPE_KEY' to keyboard key nr. 27 - ESC.

	var util = {
		uuid: function () {	// generate random id for todos.
			/*jshint bitwise:false */
			var i, random;
			var uuid = '';

			for (i = 0; i < 32; i++) {
				random = Math.random() * 16 | 0;
				if (i === 8 || i === 12 || i === 16 || i === 20) {
					uuid += '-';
				}
				uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
			}

			return uuid;
		},
		pluralize: function (count, word) { 
			return count === 1 ? word : word + 's'; // if 'count' equal to 1 is true return 'word', if false, return 'word's.
		},
		store: function (namespace, data) {
			if (arguments.length > 1) { // if the number of arguments is more than 1,
				return localStorage.setItem(namespace, JSON.stringify(data)); // stringify and save the array in local storage, return it.
			} else { // otherwise
				var store = localStorage.getItem(namespace); // set var. 'store' to data saved in local storage.
				return (store && JSON.parse(store)) || []; // if 'store' and parsing the 'store' is true return it. If it is false, return empty array.
			}
		}
	};

	var App = {
		init: function () { 
			this.todos = util.store('todos-jquery'); // set var. 'this.todos' to local storage data with 'todos-jquery' key.
			this.todoTemplate = Handlebars.compile($('#todo-template').html()); //  look for '#todo-template' element and compile it as handlebars 'todoTemplate'.
			this.footerTemplate = Handlebars.compile($('#footer-template').html()); // look for '#footer-template' element and compile it as handlebars 'footer-template'.
			this.bindEvents(); // run 'bindEvents' method.

			new Router({
				'/:filter': function (filter) { // set var. 'filter' (/:filter) to a changing value depending on URL (#/all).
					this.filter = filter; // set var. 'this.filter' to 'filter'.
					this.render(); // run 'render' method.
				}.bind(this) // bind 'this' to 'App' object.
			}).init('/all'); // on initializing the app open /all URL.
		},
		bindEvents: function () { 
			$('#new-todo').on('keyup', this.create.bind(this));	// select '#new-todo' element with jQuery. Attach 'create' method for 'keyup' event to selected element. Bind 'this' to 'App' object.
			$('#toggle-all').on('change', this.toggleAll.bind(this)); // select '#toggle-all' element with jQuery. Attach 'toggleAll' method for 'change' event to selected element. Bind 'this' to 'App' object.
			$('#footer').on('click', '#clear-completed', this.destroyCompleted.bind(this)); // select '#footer' element with jQuery. Attach 'destroyCompleted' method for 'click' event to selected elements child with id '#clear-completed'. Bind 'this' to 'App' object.
			$('#todo-list') // select '#todo-list' element with jQuery. Attach:
				.on('change', '.toggle', this.toggle.bind(this)) // 'toggle' method for 'change' event to selected elements child with class '.toggle'. Bind 'this' to 'App' object.
				.on('dblclick', 'label', this.edit.bind(this)) // 'edit' method for 'dblclick' event to selected elements child 'label' element. Bind 'this' to 'App' object.
				.on('keyup', '.edit', this.editKeyup.bind(this)) // 'editKeyup' method for 'keyup' event to selected elements child with class '.edit'. Bind 'this' to 'App' object.
				.on('focusout', '.edit', this.update.bind(this)) // 'update' method for 'focusout' event to selected elements child with class '.edit'. Bind 'this' to 'App' object.
				.on('click', '.destroy', this.destroy.bind(this)); // 'destroy' method for 'click' event to selected elements child with class '.destroy'. Bind 'this' to 'App' object.
		},
		render: function () {
			var todos = this.getFilteredTodos(); // set var. 'todos' to callback from 'getFilteredTodos'.
			$('#todo-list').html(this.todoTemplate(todos)); // pass 'todos' data to handlebars 'todoTemplate' and put it inside '#todo-list' element.
			$('#main').toggle(todos.length > 0); // flip '#main' CSS display property depending on 'todos array' lenght.
			$('#toggle-all').prop('checked', this.getActiveTodos().length === 0); // get 'checked' value of '#toggle-all' element. Set it to true, if 'getActiveTodos' lenght (count) is 0, else false.
			this.renderFooter(); // run 'renderFooter' method.
			$('#new-todo').focus(); // focus on '#new-todo' element.
			util.store('todos-jquery', this.todos); // save ''todos array' in local storage under 'todos-jquery' key.
		},
		renderFooter: function () {
			var todoCount = this.todos.length; // set var. 'todoCount' to 'todos array' lenght.
			var activeTodoCount = this.getActiveTodos().length; // set var. 'activeTodoCount' to lenght of the callback from 'getActiveTodos' method.
			var template = this.footerTemplate({ // pass data to 'footerTemplate':
				activeTodoCount: activeTodoCount, // set var. 'activeTodoCount' to 'activeTodoCount';
				activeTodoWord: util.pluralize(activeTodoCount, 'item'), //set var. 'activeTodoWord' to returned data from 'pluralize' method; 
				completedTodos: todoCount - activeTodoCount, // set var. completedTodos to 'todoCount' minus 'activeTodoCount';
				filter: this.filter // set var. 'filter' to 'this.filter/App.filter'. 		
			});
			$('#footer').toggle(todoCount > 0).html(template); // flip '#footer' CSS display property, if 'todoCount' is more than 0. Inject 'template' in '#footer' element.
		},
		toggleAll: function (e) {
			var isChecked = $(e.target).prop('checked'); // select 'event target' element with jQuery and find its 'checked' property and set var. 'isChecked' to it.

			this.todos.forEach(function (todo) { // run for each loop on 'todos array'. 
				todo.completed = isChecked; // Change todo completed property for each todo. 
			});

			this.render(); // run 'render' method.
		},
		getActiveTodos: function () {	
			return this.todos.filter(function (todo) { // return 'todos array' with only elements that pass the callback function test.
				return !todo.completed;	// return todo with completed propery 'false'.
			});
		},
		getCompletedTodos: function () { 
			return this.todos.filter(function (todo) { // return 'todos array' with only elements that pass the callback function test.
				return todo.completed; // return todo with completed property 'true'.
			});
		},
		getFilteredTodos: function () { 
			if (this.filter === 'active') { // if 'this.filter/App.filter' is set to 'active',
				return this.getActiveTodos(); // return data provided by 'getActiveTodos' method.
			}

			if (this.filter === 'completed') { // if 'this.filter/App.filter' is set to 'completed',
				return this.getCompletedTodos(); // return data provided by 'getCompletedTodos' method.
			}

			return this.todos; // if there is no 'filter' set, return 'todos array'.
		},
		destroyCompleted: function () {
			this.todos = this.getActiveTodos(); // set 'todos array' to data provided by 'getActiveTodos' method.
			this.filter = 'all'; // set 'this.filter/App.filter' to 'all'.
			this.render(); // run 'render' method.
		},
		// accepts an element from inside the `.item` div and
		// returns the corresponding index in the `todos` array
		indexFromEl: function (el) {
			var id = $(el).closest('li').data('id'); // select function argument element with jQury. Find its closest HTML 'li' element by traversing up the DOM starting with self, then check the data-id value of it and set var. 'id' to this value.
			var todos = this.todos; // set var. 'todos' to 'todos array'.
			var i = todos.length; // set var. 'i' to var. 'todos' lenght.
			while (i--) { // run a while loop with condition 'i', decrement by 1 before the code runs.
				if (todos[i].id === id) { // if 'todo' in 'todos array' with index/position 'i' has 'id' property the same as var. 'id',
					return i; // return 'i' value.
				}
			}
		},
		create: function (e) {	
			var $input = $(e.target); // set var. '$input' to jQuery selected 'event target' element.
			var val = $input.val().trim(); // set var. 'val' to '$input' elements current value with trimmed whitespaces.
			if (e.which !== ENTER_KEY || !val) { // if event key presset is not ENTER (var. 'ENTER_KEY') or var. 'val' is false,
				return; // then return.
			}

			this.todos.push({ // add new element to the end of 'todos array':
				id: util.uuid(), // set elements 'id' property to value returned by callback function 'util.uuid';
				title: val, // set elements 'title' property to var. 'val';
				completed: false // set elements 'completed' property to 'false'.
			});

			$input.val(''); // set '$input' value to empty string.

			this.render(); // run 'render' method.
		},
		toggle: function (e) {
			var i = this.indexFromEl(e.target); // set var. 'i' to value returned by 'indexFromEl' method with the 'event target' passed to it as argument.
			this.todos[i].completed = !this.todos[i].completed; // look for completed property of todo in 'todos array' with index/position of 'i' and flip it.
			this.render(); // run 'render' method.
		},
		edit: function (e) {
			var $input = $(e.target).closest('li').addClass('editing').find('.edit'); // select 'event target' element with jQuery. Find first 'li' element by traversing up through DOM and give it class '.editing'. Now look through descendants to find element with class '.edit' and make a new jQuery object and set is as var. '$input'.
			$input.val($input.val()).focus(); // check var. '$input' current value and change it to '$input' current value, and focus on it.
		},
		editKeyup: function (e) {	
			if (e.which === ENTER_KEY) { // if event key pressed is ENTER (var. 'ENTER_KEY'),
				e.target.blur(); // lose focus from 'event target'.
			}

			if (e.which === ESCAPE_KEY) { // if event key pressed is ESC (var. ESCAPE_KEY),
				$(e.target).data('abort', true).blur(); // store arbitrary data "'abort', true" on jQuery selected 'event target' element, then lose focus of it.
			}
		},
		update: function (e) {
			var el = e.target; // set var. 'el' to 'event target'.
			var $el = $(el); // set var. '$el' to jQuery selected var 'el'.
			var val = $el.val().trim(); // set var. 'val' to var. '$el' currecnt value with trimmed whitespaces.

			if (!val) { // if 'val' is false,
				this.destroy(e); // run destroy method,
				return; // then return.
			}

			if ($el.data('abort')) { // if '$el' has arbitrary data 'abort',
				$el.data('abort', false); // change it to "'abort', false".
			} else {
				this.todos[this.indexFromEl(el)].title = val; // else change todo, with index/position callback from 'indexFromEl' title to var. 'val'. 
			}

			this.render(); // run 'render' method.
		},
		destroy: function (e) { 
			this.todos.splice(this.indexFromEl(e.target), 1); // on 'todos array' run splice method. First argument passed to it is a callback from 'indexFromEl' method, second is 1.
			this.render(); // run 'render' method.
		}
	};

	App.init();	// run 'init' method.
});